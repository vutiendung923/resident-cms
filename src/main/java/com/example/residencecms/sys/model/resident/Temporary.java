package com.example.residencecms.sys.model.resident;

import com.example.residencecms.sys.model.population.Mounth;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "TEMPORARY")
@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
@SequenceGenerator(name = "SEQ", sequenceName = "SEQ", allocationSize = 1)
public class Temporary {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ")
    private Integer id;
    private String tempAddress;
    private String tempReason;
    private String accompanyChild;
    private String houseHolder;
    private String holderRelation;
    private Date fromTime;
    private Date toTime;
    private Integer officerConfirm;
    private Integer holderConfirm;

    @ManyToOne
    @JoinColumn(name = "MOUTH_ID")
    private Mounth mounth;
}
