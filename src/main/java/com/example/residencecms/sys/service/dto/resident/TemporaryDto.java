package com.example.residencecms.sys.service.dto.resident;

import com.example.residencecms.sys.model.population.Mounth;
import com.example.residencecms.sys.model.resident.Temporary;
import com.example.residencecms.sys.service.dto.population.MounthDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class TemporaryDto {
    private Integer id;
    @NotNull
    private MounthDto mounth;
    private String tempAddress;
    private String temptReason;
    private String accompanyChild;
    private String houseHolder;
    private String holderRelation;
    private Date fromTime;
    private Date toTime;
    private Integer officerConfirm;
    private Integer holderConfirm;

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        private Integer id;
        private MounthDto mounth;
        private String tempAddress;
        private String temptReason;
        private String accompanyChild;
        private String houseHolder;
        private String holderRelation;
        private Date fromTime;
        private Date toTime;
        private Integer officerConfirm;
        private Integer holderConfirm;

        public Builder temporary(Temporary temporary) {
            this.id = temporary.getId();
            if(temporary.getMounth() != null) {
                this.mounth = new MounthDto(temporary.getMounth());
            }
            this.tempAddress = temporary.getTempAddress();
            this.temptReason = temporary.getTempReason();
            this.accompanyChild = temporary.getAccompanyChild();
            this.houseHolder = temporary.getHouseHolder();
            this.holderRelation = temporary.getHolderRelation();
            this.fromTime = temporary.getFromTime();
            this.toTime = temporary.getToTime();;
            this.officerConfirm = temporary.getOfficerConfirm();
            this.holderConfirm= temporary.getHolderConfirm();
            return this;
        }

        public TemporaryDto build(){
            return new TemporaryDto(id, mounth, tempAddress, temptReason, accompanyChild, houseHolder, holderRelation,
                    fromTime, toTime, officerConfirm, holderConfirm);
        }
    }
}
