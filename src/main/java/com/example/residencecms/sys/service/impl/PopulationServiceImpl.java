package com.example.residencecms.sys.service.impl;

import com.example.residencecms.common.PageData;
import com.example.residencecms.sys.model.population.Population;
import com.example.residencecms.sys.repository.PopulationRepo;
import com.example.residencecms.sys.service.PopulationService;
import com.example.residencecms.sys.service.dto.population.PopulationDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class PopulationServiceImpl implements PopulationService {

    private final PopulationRepo populationRepo;

    public PopulationServiceImpl(PopulationRepo populationRepo) {
        this.populationRepo = populationRepo;
    }


    @Override
    @Transactional
    public PageData<PopulationDto> getListPopulation(Integer pageIndex, Integer pageSize) {
        Specification<Population> specification = (root, cq, cb) -> {
            return cb.and();
        };
        Page<Population> populationPage = populationRepo.findAll(specification,
                PageRequest.of(pageIndex-1, pageSize));
        List<PopulationDto> populationDtoList = populationPage.getContent()
                .stream()
                .map(population -> PopulationDto.builder().population(population).build())
                .collect(Collectors.toList());
        return PageData.of(populationPage, populationDtoList);
    }

    @Override
    @Transactional
    public PopulationDto getDetailPopulation(Integer id) {
        return PopulationDto.builder().population(
                populationRepo.findById(id).orElseThrow(RuntimeException::new)
        ).build();
    }

    @Override
    @Transactional
    public void addPopulation(PopulationDto populationDto) {
        Population population = new Population();
        population.setPopulationCode(populationDto.getPopulationCode());
        population.setAddress(populationDto.getAddress());
        population.setHouseHolder(populationDto.getHouseHolder());
        population.setIssueDay(populationDto.getIssueDay());
        populationRepo.save(population);
    }

    @Override
    @Transactional
    public void updatePopulation(PopulationDto populationDto) {
        Population population = populationRepo.findById(populationDto.getId()).orElseThrow(RuntimeException::new);
        population.setPopulationCode(populationDto.getPopulationCode());
        population.setAddress(populationDto.getAddress());
        population.setHouseHolder(populationDto.getHouseHolder());
        population.setIssueDay(populationDto.getIssueDay());
        populationRepo.save(population);
    }

    @Override
    @Transactional
    public void deletePopulation(Integer id) {
        populationRepo.deleteById(id);
    }
}
