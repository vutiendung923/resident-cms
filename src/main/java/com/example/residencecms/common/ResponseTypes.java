package com.example.residencecms.common;

public class ResponseTypes {
    public static final ResponseError SUCCESS = new ResponseError(0, "SUCCESS");
    public static final ResponseError ERROR = new ResponseError(99, "ERROR");
    public static final ResponseError BAD_REQUEST = new ResponseError(1, "BAD_REQUEST");
    public static final ResponseError DB_ERROR = new ResponseError(2, "DB_ERROR");
    public static final ResponseError DB_RETRY_ERROR = new ResponseError(2, "DB_RETRY_ERROR");
    public static final ResponseError TOKEN_INVALID = new ResponseError(3, "TOKEN_INVALID");
    public static final ResponseError REST_TIME = new ResponseError(4, "REST_TIME");
    public static final ResponseError PRICE_NOT_ACCEPT = new ResponseError(5, "PRICE_NOT_ACCEPT");
}
