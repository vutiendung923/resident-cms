package com.example.residencecms.sys.controller;

import com.example.residencecms.common.ResponseEntity;
import com.example.residencecms.common.ResponseTypes;
import com.example.residencecms.config.inject.CurrentRequestId;
import com.example.residencecms.sys.service.TemporaryService;
import com.example.residencecms.sys.service.dto.resident.TemporaryDto;
import org.springframework.stereotype.Component;

import javax.validation.Valid;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import java.util.Date;

@Component
@Valid
@Path("temporary")
public class TemporaryController {

    @CurrentRequestId
    String requestId;

    private final TemporaryService temporaryService;

    public TemporaryController(TemporaryService temporaryService) {
        this.temporaryService = temporaryService;
    }

    @GET
    @Path("list")
    public Response getListTemporary(@QueryParam("pageIndex") Integer pageIndex,
                              @QueryParam("pageSize") Integer pageSize) {
        return Response.ok(new ResponseEntity(requestId, new Date(),
                ResponseTypes.SUCCESS, temporaryService.getListTemporary(pageIndex, pageSize))).build();
    }

    @GET
    @Path("detail")
    public Response getDetailTemporary(@QueryParam("id") Integer id) {
        return Response.ok(new ResponseEntity(requestId, new Date(),
                ResponseTypes.SUCCESS, temporaryService.getDetailTemporary(id))).build();
    }

    @POST
    @Path("add")
    public Response addTemporary(TemporaryDto temporaryDto) {
        temporaryService.addTemporary(temporaryDto);
        return Response.ok(new ResponseEntity(requestId, new Date(),
                ResponseTypes.SUCCESS)).build();
    }

    @POST
    @Path("update")
    public Response updateTemporary(TemporaryDto temporaryDto) {
        temporaryService.updateTemporary(temporaryDto);
        return Response.ok(new ResponseEntity(requestId, new Date(),
                ResponseTypes.SUCCESS)).build();
    }

    @GET
    @Path("delete")
    public Response deleteTemporary(@QueryParam("id") Integer id) {
        temporaryService.deleteTemporary(id);
        return Response.ok(new ResponseEntity(requestId, new Date(),
                ResponseTypes.SUCCESS)).build();
    }
}
