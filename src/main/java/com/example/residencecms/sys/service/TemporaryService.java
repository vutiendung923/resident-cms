package com.example.residencecms.sys.service;

import com.example.residencecms.common.PageData;
import com.example.residencecms.sys.service.dto.resident.TemporaryDto;

public interface TemporaryService {
    PageData<TemporaryDto> getListTemporary(Integer pageIndex, Integer pageSize);
    TemporaryDto getDetailTemporary(Integer id);
    void addTemporary(TemporaryDto temporaryDto);
    void updateTemporary(TemporaryDto temporaryDto);
    void deleteTemporary(Integer id);
}
