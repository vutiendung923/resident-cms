package com.example.residencecms.sys.service.dto.population;

import com.example.residencecms.sys.model.population.Mounth;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;
import javax.validation.constraints.NotBlank;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MounthDto {
    private Integer id;

    @NotBlank
    @Length(min = 5)
    private String name;

    private String differentName;
    private String birthDay;
    private String nativePlace;
    private String residentPopulation;
    private String nation;
    private String nowAddress;
    private String learning;
    private String foreignLanguage;
    private Integer officerConfirm;
    private Integer houseHolderConfirm;
    private Integer personConfirm;

    public MounthDto(Mounth mounth) {
        this.id = mounth.getId();
        this.name = mounth.getName();
        this.differentName = mounth.getDifferentName();
        this.birthDay = mounth.getBirthDay();
        this.nativePlace = mounth.getNativePlace();
        this.residentPopulation = mounth.getResidentPopulation();
        this.nation = mounth.getNation();
        this.nowAddress = mounth.getNowAddress();
        this.learning = mounth.getLearning();
        this.foreignLanguage = mounth.getForeignLanguage();
        this.officerConfirm = mounth.getOfficerConfirm();
        this.houseHolderConfirm = mounth.getHouseHolderConfirm();
        this.personConfirm = mounth.getPersonConfirm();
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        private Integer id;
        private String name;
        private String differentName;
        private String birthDay;
        private String nativePlace;
        private String residentPopulation;
        private String nation;
        private String nowAddress;
        private String learning;
        private String foreignLanguage;
        private Integer officerConfirm;
        private Integer houseHolderConfirm;
        private Integer personConfirm;

        public Builder mounth(Mounth mounth) {
            this.id = mounth.getId();
            this.name = mounth.getName();
            this.differentName = mounth.getDifferentName();
            this.birthDay = mounth.getBirthDay();
            this.nativePlace = mounth.getNativePlace();
            this.residentPopulation = mounth.getResidentPopulation();
            this.nation = mounth.getNation();
            this.nowAddress = mounth.getNowAddress();
            this.learning = mounth.getLearning();
            this.foreignLanguage = mounth.getForeignLanguage();
            this.officerConfirm = mounth.getOfficerConfirm();
            this.houseHolderConfirm = mounth.getHouseHolderConfirm();
            this.personConfirm = mounth.getPersonConfirm();
            return this;
        }

        public MounthDto build() {
            return new MounthDto(id, name, differentName, birthDay, nativePlace, residentPopulation,
                    nation, nowAddress, learning, foreignLanguage, officerConfirm, houseHolderConfirm,
                    personConfirm);
        }
    }
}
