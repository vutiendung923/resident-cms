package com.example.residencecms.sys.controller;

import com.example.residencecms.common.ResponseEntity;
import com.example.residencecms.common.ResponseTypes;
import com.example.residencecms.config.inject.CurrentRequestId;
import com.example.residencecms.sys.model.population.Population;
import com.example.residencecms.sys.service.PopulationService;
import com.example.residencecms.sys.service.dto.population.MounthDto;
import com.example.residencecms.sys.service.dto.population.PopulationDto;
import org.springframework.stereotype.Component;

import javax.validation.Valid;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import java.util.Date;

@Component
@Valid
@Path("population")
public class PopulationController {
    @CurrentRequestId
    String requestId;

    private final PopulationService populationService;

    public PopulationController(PopulationService populationService) {
        this.populationService = populationService;
    }

    @GET
    @Path("list")
    public Response getListPopulation(@QueryParam("pageIndex") Integer pageIndex,
                                  @QueryParam("pageSize") Integer pageSize){
        return Response.ok(new ResponseEntity(requestId, new Date(),
                ResponseTypes.SUCCESS, populationService.getListPopulation(pageIndex, pageSize))).build();
    }

    @GET
    @Path("detail")
    public Response getDetailPopulation(@QueryParam("id") Integer id){
        return Response.ok(new ResponseEntity(requestId, new Date(),
                ResponseTypes.SUCCESS, populationService.getDetailPopulation(id))).build();
    }

    @POST
    @Path("add")
    public Response addPopulation(PopulationDto populationDto){
        populationService.addPopulation(populationDto);
        return Response.ok(new ResponseEntity(requestId, new Date(),
                ResponseTypes.SUCCESS)).build();
    }

    @POST
    @Path("update")
    public Response updatePopulation(PopulationDto populationDto){
        populationService.updatePopulation(populationDto);
        return Response.ok(new ResponseEntity(requestId, new Date(),
                ResponseTypes.SUCCESS)).build();
    }

    @GET
    @Path("delete")
    public Response deletePopulation(@QueryParam("id") Integer id){
        populationService.deletePopulation(id);
        return Response.ok(new ResponseEntity(requestId, new Date(),
                ResponseTypes.SUCCESS)).build();
    }
}
