package com.example.residencecms.sys.controller;

import com.example.residencecms.common.ResponseEntity;
import com.example.residencecms.common.ResponseTypes;
import com.example.residencecms.common.Roles;
import com.example.residencecms.config.inject.CurrentRequestId;
import com.example.residencecms.sys.service.AccountService;
import com.example.residencecms.sys.service.dto.AccountDto;
import org.springframework.stereotype.Component;

import javax.validation.Valid;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import java.util.Date;

@Component
@Valid
@Path("account")
public class AccountController {

    @CurrentRequestId
    private String requestId;

    private final AccountService accountService;

    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }


    @POST
    @Path("add")
    public Response addAccount(AccountDto accountDto){
        accountService.addAccount(accountDto);
        return Response.ok(new ResponseEntity(requestId, new Date(), ResponseTypes.SUCCESS)).build();
    }

    @POST
    @Path("login")
    public Response loginAccount(AccountDto accountDto){
        return Response.ok(new ResponseEntity(requestId, new Date(), ResponseTypes.SUCCESS,
                accountService.loginAccount(accountDto))).build();
    }

    @GET
    public Response getListAccount(@QueryParam("pageIndex") Integer pageIndex,
                                   @QueryParam("pageSize") Integer pageSize,
                                   @QueryParam("username") String username,
                                   @QueryParam("status") Integer accountStatus) {
        return Response.ok(new ResponseEntity(requestId, new Date(), ResponseTypes.SUCCESS,
                accountService.getListAccount(pageIndex, pageSize, username, accountStatus))).build();
    }

    @GET
    @Path("detail")
    public Response getDetailAccount(@QueryParam("id") Integer id) {
        return Response.ok(new ResponseEntity(requestId, new Date(), ResponseTypes.SUCCESS,
                accountService.getDetailAccount(id))).build();
    }

    @POST
    @Path("update")
    public Response updateAccount(AccountDto accountDto){
        accountService.updateAccount(accountDto);
        return Response.ok(new ResponseEntity(requestId, new Date(), ResponseTypes.SUCCESS)).build();
    }

    @GET
    @Path("delete")
    public Response deleteAccount(@QueryParam("id") Integer id){
        accountService.deleteAccount(id);
        return Response.ok(new ResponseEntity(requestId, new Date(), ResponseTypes.SUCCESS)).build();
    }
}
