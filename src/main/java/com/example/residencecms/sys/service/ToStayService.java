package com.example.residencecms.sys.service;

import com.example.residencecms.common.PageData;
import com.example.residencecms.sys.service.dto.resident.ToStayDto;

public interface ToStayService {
    PageData<ToStayDto> getListToStay(Integer pageIndex, Integer pageSize);
    ToStayDto getDetailToStay(Integer id);
    void addToStay(ToStayDto toStayDto);
    void updateToStay(ToStayDto toStayDto);
    void deleteToStay(Integer id);
}
