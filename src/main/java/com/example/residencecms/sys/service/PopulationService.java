package com.example.residencecms.sys.service;

import com.example.residencecms.common.PageData;
import com.example.residencecms.sys.service.dto.population.PopulationDto;

public interface PopulationService {
    PageData<PopulationDto> getListPopulation(Integer pageIndex, Integer pageSize);
    PopulationDto getDetailPopulation(Integer id);
    void addPopulation(PopulationDto populationDto);
    void updatePopulation(PopulationDto populationDto);
    void deletePopulation(Integer id);
}
