package com.example.residencecms.config.filter;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.glassfish.jersey.logging.LoggingFeature;
import org.glassfish.jersey.message.internal.ReaderWriter;

import javax.annotation.Priority;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.PreMatching;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;

import static com.example.residencecms.common.Constant.*;


@Provider
@PreMatching
@Priority(1)
public class CustomLoggingRequestFilter extends LoggingFeature implements ContainerRequestFilter {
    private static final Log log = LogFactory.getLog(CustomLoggingRequestFilter.class);

    @Context
    private HttpServletRequest httpServletRequest;

    @Context
    private HttpServletResponse httpServletResponse;

    private static boolean isPreflightRequest(ContainerRequestContext request) {
        return request.getHeaderString("Origin") != null && request.getMethod().equalsIgnoreCase("OPTIONS");
    }

    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {

        if (isPreflightRequest(requestContext)) {
            requestContext.abortWith(Response.ok().build());
        }

        String requestId = UUID.randomUUID().toString();
        httpServletRequest.setAttribute(TRACING_LOG_ATTRIBUTE_NAME, requestId);
        httpServletRequest.setAttribute(MONITOR_LOG_ATTRIBUTE_NAME, System.currentTimeMillis());

        requestContext.setProperty(TRACING_LOG_ATTRIBUTE_NAME, requestId);
        requestContext.setProperty(TRACING_IP_ATTRIBUTE_NAME, getRemoteIp());

        StringBuilder sb = new StringBuilder();
        sb.append("RequestId: ").append(requestId);
        sb.append(" - Remote IP: ").append(getRemoteIp());
        sb.append(" - Path: ").append(requestContext.getUriInfo().getPath());
        sb.append(" - Header: ").append(requestContext.getHeaders());
        sb.append(" - QueryString: ").append(httpServletRequest.getQueryString());
        sb.append(" - Entity: ").append(getEntityBody(requestContext));
        log.info("HTTP REQUEST ==> " + sb.toString());
    }

    private String getRemoteIp() {
        String remoteIp = httpServletRequest.getHeader("X-FORWARDED-FOR");
        if (StringUtils.isBlank(remoteIp)) {
            return httpServletRequest.getRemoteAddr();
        }
        return remoteIp;
    }

    private String getEntityBody(ContainerRequestContext requestContext) {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        InputStream in = requestContext.getEntityStream();

        final StringBuilder b = new StringBuilder();
        try {
            ReaderWriter.writeTo(in, out);

            byte[] requestEntity = out.toByteArray();
            if (requestEntity.length == 0) {
                b.append("\n");
            } else {
                b.append(new String(requestEntity)).append("\n");
            }
            requestContext.setEntityStream(new ByteArrayInputStream(requestEntity));

        } catch (IOException ex) {
            log.error(String.format("RequestId: %s, get entity body throw exception %s cause by %s",
                    httpServletRequest.getAttribute(TRACING_LOG_ATTRIBUTE_NAME), ex.getMessage(), ex.getCause()), ex);
        }
        return b.toString();
    }
}
