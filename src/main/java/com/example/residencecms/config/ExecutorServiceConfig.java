package com.example.residencecms.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Configuration
public class ExecutorServiceConfig {
    @Bean
    public ExecutorService getFixedThreadExecutor() {
        return Executors.newFixedThreadPool(25);
    }

    @Bean
    public ExecutorService getCachedThreadExecutor() {
        return Executors.newCachedThreadPool();
    }

    @Bean
    public ExecutorService getSingleThreadExecutor() {
        return Executors.newSingleThreadExecutor();
    }
}
