package com.example.residencecms.common;

public interface Roles {
    String ADMIN = "ADMIN";
    String EMPLOYEE = "EMPLOYEE";
}
