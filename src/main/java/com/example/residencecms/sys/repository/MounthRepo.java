package com.example.residencecms.sys.repository;

import com.example.residencecms.sys.model.population.Mounth;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface MounthRepo extends JpaRepository<Mounth, Integer>, JpaSpecificationExecutor<Mounth> {

}
