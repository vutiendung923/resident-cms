package com.example.residencecms.sys.service.dto.population;

import com.example.residencecms.sys.model.population.Population;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.sql.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class PopulationDto {
    private Integer id;
    private String houseHolder;
    private String populationCode;
    private String address;
    private Date issueDay;

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder{
        private Integer id;
        private String houseHolder;
        private String populationCode;
        private String address;
        private Date issueDay;

        public Builder population(Population population){
            this.id = population.getId();
            this.houseHolder = population.getHouseHolder();
            this.populationCode = population.getPopulationCode();
            this.address = population.getAddress();
            this.issueDay = population.getIssueDay();
            return this;
        }

        public PopulationDto build() {
            return new PopulationDto(id, houseHolder, populationCode, address, issueDay);
        }
    }
}
