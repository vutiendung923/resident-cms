package com.example.residencecms.sys.repository;

import com.example.residencecms.sys.model.resident.ToStay;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface ToStayRepo extends JpaRepository<ToStay, Integer>, JpaSpecificationExecutor<ToStay> {
}
