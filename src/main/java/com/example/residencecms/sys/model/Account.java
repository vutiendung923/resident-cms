package com.example.residencecms.sys.model;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "ACCOUNT")
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Data
@SequenceGenerator(name = "ACCOUNT_SEQUENCE", sequenceName = "ACCOUNT_SEQUENCE", allocationSize = 1)
public class Account {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ACCOUNT_SEQUENCE")
    private Integer id;
    private String username;
    private String password;
    private String accessToken;
    private Integer accountStatus;
    private String role;

    @ManyToOne
    @JoinColumn(name = "GROUP_ID")
    private GroupAccount groupAccount;
}
