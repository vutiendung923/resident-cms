package com.example.residencecms.sys.service.dto.resident;

import com.example.residencecms.sys.model.population.Mounth;
import com.example.residencecms.sys.model.resident.Absent;
import com.example.residencecms.sys.service.dto.population.MounthDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class AbsentDto {
    private Integer id;

    @NotNull
    private MounthDto mounth;

    private String profession;
    private String absentAddress;
    private String absentReason;
    private String accompanyChild;
    private String houseHolder;
    private String holderRelation;
    private Date fromTime;
    private Date toTime;
    private Integer officerConfirm;
    private Integer holderConfirm;

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        private Integer id;
        private MounthDto mounth;
        private String profession;
        private String absentAddress;
        private String absentReason;
        private String accompanyChild;
        private String houseHolder;
        private String holderRelation;
        private Date fromTime;
        private Date toTime;
        private Integer officerConfirm;
        private Integer holderConfirm;

        public Builder absent(Absent absent) {
            this.id = absent.getId();
            if(absent.getMounth() != null) {
                this.mounth = new MounthDto(absent.getMounth());
            }
            this.profession = absent.getProfession();
            this.absentAddress = absent.getAbsentAddress();
            this.absentReason = absent.getAbsentReason();
            this.accompanyChild = absent.getAccompanyChild();
            this.houseHolder = absent.getHouseHolder();
            this.holderRelation = absent.getHolderRelation();
            this.fromTime = absent.getFromTime();
            this.toTime = absent.getToTime();
            this.officerConfirm = absent.getOfficerConfirm();
            this.holderConfirm = absent.getHolderConfirm();
            return this;
        }

        public AbsentDto build() {
            return new AbsentDto(id, mounth, profession, absentAddress, absentReason, accompanyChild,
                    houseHolder, holderRelation, fromTime, toTime, officerConfirm, holderConfirm);
        }
    }
}
