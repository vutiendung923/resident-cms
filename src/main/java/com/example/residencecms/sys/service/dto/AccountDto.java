package com.example.residencecms.sys.service.dto;

import com.example.residencecms.sys.model.Account;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class AccountDto {
    private Integer id;
    private String username;
    private String password;
    private String accessToken;
    private String group;
    private Integer accountStatus;

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder{
        private Integer id;
        private String username;
        private String password;
        private String accessToken;
        private String group;
        private Integer accountStatus;

        public Builder account(Account account){
            this.id = account.getId();
            this.username = account.getUsername();
            this.password = account.getPassword();
            this.accessToken = account.getAccessToken();;
            if(account.getGroupAccount() != null) {
                this.group = account.getGroupAccount().getGroupName();
            }
            this.accountStatus = account.getAccountStatus();
            return this;
        }

        public AccountDto build(){
            return new AccountDto(id, username, password, accessToken, group, accountStatus);
        }
    }
}
