package com.example.residencecms.config;

import org.ehcache.Cache;
import org.ehcache.CacheManager;
import org.ehcache.config.builders.CacheConfigurationBuilder;
import org.ehcache.config.builders.CacheManagerBuilder;
import org.ehcache.config.builders.ResourcePoolsBuilder;
import org.ehcache.expiry.Duration;
import org.ehcache.expiry.Expirations;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.servlet.http.HttpSession;
import java.util.concurrent.TimeUnit;

@Configuration
public class CacheConfig {
    private final Cache<HttpSession, Long> spamManager;

    public CacheConfig() {
        CacheManager cacheManager = CacheManagerBuilder
                .newCacheManagerBuilder().build();
        cacheManager.init();

        spamManager = cacheManager
                .createCache("spamManager", CacheConfigurationBuilder
                        .newCacheConfigurationBuilder(HttpSession.class, Long.class,
                                ResourcePoolsBuilder.heap(100))
                        .withExpiry(Expirations.timeToLiveExpiration(Duration.of(5,
                                TimeUnit.MINUTES))).build());
    }

    @Bean
    public Cache<HttpSession, Long> getSpamManager() {
        return spamManager;
    }
}
