package com.example.residencecms.sys.model.population;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;

@Entity
@Table(name = "POPULATION")
@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
@SequenceGenerator(name = "SEQ", sequenceName = "SEQ", allocationSize = 1)
public class Population {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ")
    private Integer id;
    private String houseHolder;
    private String populationCode;
    private String address;
    private Date issueDay;
//
//    @OneToMany(mappedBy = "population")
//    private List<Mounth> mounth;
}
