package com.example.residencecms.sys.service;


import com.example.residencecms.common.Auth;

public interface JwtService {
    String encode(Auth auth, int hourNumber);

    Auth decode(String jwt);

    boolean isExpiredToken(String token);
}
