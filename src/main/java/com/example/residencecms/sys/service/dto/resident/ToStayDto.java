package com.example.residencecms.sys.service.dto.resident;

import com.example.residencecms.sys.model.resident.ToStay;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class ToStayDto {
    private Integer id;

    public static Builder builder(){
        return new Builder();
    }

    public static class Builder{
        private Integer id;

        public Builder toStay(ToStay toStay) {
            this.id = toStay.getId();
            return this;
        }

        public ToStayDto build() {
            return new ToStayDto(id);
        }
    }
}
