package com.example.residencecms.config;


import com.example.residencecms.common.Auth;

import javax.ws.rs.core.SecurityContext;
import java.security.Principal;

public class CustomSecurityContext implements SecurityContext {
    private Auth auth;
    private boolean secure;

    public CustomSecurityContext(Auth auth, boolean secure) {
        this.auth = auth;
        this.secure = secure;
    }

    @Override
    public Principal getUserPrincipal() {
        return () -> auth.getUserName();
    }

    @Override
    public boolean isUserInRole(String s) {
        return auth.getRoles().contains(s);
    }

    @Override
    public boolean isSecure() {
        return secure;
    }

    @Override
    public String getAuthenticationScheme() {
        return SecurityContext.BASIC_AUTH;
    }
}
