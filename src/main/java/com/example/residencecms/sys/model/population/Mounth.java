package com.example.residencecms.sys.model.population;

import com.example.residencecms.sys.model.resident.Absent;
import com.example.residencecms.sys.model.resident.Temporary;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "MOUNTH")
@Data
@ToString
@NoArgsConstructor
@SequenceGenerator(name = "SEQ", sequenceName = "SEQ", allocationSize = 1)
public class Mounth {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ")
    private Integer id;
    private String name;
    private String differentName;
    private String birthDay;
    private String nativePlace;
    private String residentPopulation;
    private String nation;
    private String nowAddress;
    private String learning;
    private String foreignLanguage;
    private Integer officerConfirm;
    private Integer houseHolderConfirm;
    private Integer personConfirm;

    @OneToMany(mappedBy = "mounth")
    private List<Absent> absent;

    @OneToMany(mappedBy = "mounth")
    private List<Temporary> temporary;

    public Mounth(Integer id) {
        this.id = id;
    }

//    @ManyToOne
//    @JoinColumn(name = "POPULATION_ID")
//    private Population population;
}
