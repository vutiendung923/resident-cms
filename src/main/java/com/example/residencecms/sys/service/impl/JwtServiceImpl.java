package com.example.residencecms.sys.service.impl;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.example.residencecms.common.Auth;
import com.example.residencecms.sys.service.JwtService;
import com.google.gson.Gson;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.Date;

@Service
public class JwtServiceImpl implements JwtService {

    private final String JWT_SECRET_KEY;

    @Autowired
    public JwtServiceImpl(@Value("256") String JWT_SECRET_KEY) {
        this.JWT_SECRET_KEY = JWT_SECRET_KEY;
    }

    @SneakyThrows
    @Override
    public String encode(Auth auth, int hourNumber) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.HOUR_OF_DAY, hourNumber);
        Algorithm algorithm = Algorithm.HMAC512(JWT_SECRET_KEY);
        return JWT.create()
                .withExpiresAt(calendar.getTime())
                .withClaim("user_data", new Gson().toJson(auth))
                .sign(algorithm);
    }

    @SneakyThrows
    @Override
    public Auth decode(String jwt) {
        Algorithm algorithm = Algorithm.HMAC512(JWT_SECRET_KEY);
        JWTVerifier verifier = JWT.require(algorithm).build();
        DecodedJWT decodedJWT = verifier.verify(jwt);
        return new Gson().fromJson(decodedJWT.getClaim("user_data").asString(), Auth.class);
    }

    @SneakyThrows
    @Override
    public boolean isExpiredToken(String token) {
        Algorithm algorithm = Algorithm.HMAC512(JWT_SECRET_KEY);
        JWTVerifier verifier = JWT.require(algorithm).build();
        DecodedJWT decodedJWT = verifier.verify(token);
        Date expireDate = decodedJWT.getExpiresAt();
        return new Date().after(expireDate);
    }
}
