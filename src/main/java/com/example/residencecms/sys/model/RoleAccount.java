package com.example.residencecms.sys.model;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "ROLE_ACCOUNT")
@Data
@AllArgsConstructor
@NoArgsConstructor
@SequenceGenerator(name = "SEQ", sequenceName = "SEQ", allocationSize = 1)
public class RoleAccount {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ")
    private Integer id;
    private String roleName;
}
