package com.example.residencecms.sys.service;

import javax.servlet.http.HttpSession;

public interface SpamService {
    boolean spamPerSecond(HttpSession session);
}
