package com.example.residencecms.sys.service.dto;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class LoginResponseDto {
    private Integer id;
    private String username;
    private String role;
    private String group;
    private String token;
    private String refreshToken;
}
