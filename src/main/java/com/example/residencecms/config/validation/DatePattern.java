package com.example.residencecms.config.validation;


import com.example.residencecms.config.validation.impl.DatePatternValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = {DatePatternValidator.class})
public @interface DatePattern {
    String message() default "Datetime pattern is not valid";

    String pattern() default "dd/MM/yyyy HH:mm:ss";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
