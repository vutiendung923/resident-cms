package com.example.residencecms.sys.service.impl;

import com.example.residencecms.common.PageData;
import com.example.residencecms.sys.model.population.Mounth;
import com.example.residencecms.sys.repository.AbsentRepo;
import com.example.residencecms.sys.repository.MounthRepo;
import com.example.residencecms.sys.repository.TemporaryRepo;
import com.example.residencecms.sys.service.MounthService;
import com.example.residencecms.sys.service.dto.population.MounthDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class MounthServiceImpl implements MounthService {

    private final AbsentRepo absentRepo;

    private final TemporaryRepo temporaryRepo;

    private final MounthRepo mounthRepo;

    public MounthServiceImpl(AbsentRepo absentRepo, TemporaryRepo temporaryRepo, MounthRepo mounthRepo) {
        this.absentRepo = absentRepo;
        this.temporaryRepo = temporaryRepo;
        this.mounthRepo = mounthRepo;
    }

    @Override
    public PageData<MounthDto> getListMounth(Integer pageIndex, Integer pageSize) {
        Specification<Mounth> specification = (root, cq, cb) -> {
          return cb.and();
        };
        Page<Mounth> mounthPage = mounthRepo.findAll(specification,
                PageRequest.of(pageIndex-1, pageSize));
        List<MounthDto> mounthDtoList = mounthPage.getContent()
                .stream()
                .map(mounth -> MounthDto.builder().mounth(mounth).build())
                .collect(Collectors.toList());
        return PageData.of(mounthPage, mounthDtoList);
    }

    @Override
    public MounthDto getDetailMounth(Integer id) {
        return MounthDto.builder().mounth(
                mounthRepo.findById(id).orElseThrow(RuntimeException::new)
        ).build();
    }

    @Override
    public void addMounth(MounthDto mounthDto) {
        Mounth mounth = new Mounth();
        if(mounthDto.getName() == null || mounthDto.getName() == ""){
            throw null;
        }else {
            mounth.setName(mounthDto.getName());
        }
        mounth.setBirthDay(mounthDto.getBirthDay());
        mounth.setDifferentName(mounthDto.getDifferentName());
        mounth.setNation(mounthDto.getNation());
        mounth.setLearning(mounthDto.getLearning());
        mounth.setNowAddress(mounthDto.getNowAddress());
        mounth.setNativePlace(mounthDto.getNativePlace());
        mounth.setResidentPopulation(mounthDto.getResidentPopulation());
        mounth.setHouseHolderConfirm(mounthDto.getHouseHolderConfirm());
        mounth.setOfficerConfirm(mounthDto.getOfficerConfirm());
        mounth.setPersonConfirm(mounthDto.getPersonConfirm());
        mounthRepo.save(mounth);
    }

    @Override
    public void updateMounth(MounthDto mounthDto) {
        Mounth mounth = mounthRepo.findById(mounthDto.getId()).orElseThrow(RuntimeException::new);
        mounth.setName(mounthDto.getName());
        mounth.setBirthDay(mounthDto.getBirthDay());
        mounth.setDifferentName(mounthDto.getDifferentName());
        mounth.setNation(mounthDto.getNation());
        mounth.setLearning(mounthDto.getLearning());
        mounth.setNowAddress(mounthDto.getNowAddress());
        mounth.setNativePlace(mounthDto.getNativePlace());
        mounth.setResidentPopulation(mounthDto.getResidentPopulation());
        mounth.setHouseHolderConfirm(mounthDto.getHouseHolderConfirm());
        mounth.setOfficerConfirm(mounthDto.getOfficerConfirm());
        mounth.setPersonConfirm(mounthDto.getPersonConfirm());
        mounthRepo.save(mounth);
    }

    @Override
    @Transactional(rollbackOn = Exception.class)
    public void deleteMounth(Integer id) {
        Optional<Mounth> mounthOptional = mounthRepo.findById(id);
        Mounth mounth = mounthOptional.get();
        deleteAllForeign(mounth);
        mounthRepo.delete(mounth);
    }

    private void deleteAllForeign(Mounth mounth) {
        absentRepo.deleteAll(mounth.getAbsent());
        temporaryRepo.deleteAll(mounth.getTemporary());
    }
}
