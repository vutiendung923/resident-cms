package com.example.residencecms.sys.repository;

import com.example.residencecms.sys.model.resident.Absent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface AbsentRepo extends JpaRepository<Absent, Integer>, JpaSpecificationExecutor<Absent> {
    void deleteAbsentByMounth_Id(Integer mounthId);
}
