package com.example.residencecms.sys.service;

import com.example.residencecms.common.PageData;
import com.example.residencecms.sys.service.dto.AccountDto;
import com.example.residencecms.sys.service.dto.LoginResponseDto;

public interface AccountService {
    void addAccount(AccountDto accountDto);
    LoginResponseDto loginAccount(AccountDto accountDto);
    PageData<AccountDto> getListAccount(Integer pageIndex, Integer pageSize, String username,
                                        Integer accountStatus);
    AccountDto getDetailAccount(Integer id);
    void updateAccount(AccountDto accountDto);
    void deleteAccount(Integer id);
}
