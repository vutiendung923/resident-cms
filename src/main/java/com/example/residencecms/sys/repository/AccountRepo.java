package com.example.residencecms.sys.repository;

import com.example.residencecms.sys.model.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.Optional;

public interface AccountRepo extends JpaRepository<Account, Integer>, JpaSpecificationExecutor<Account> {
    Optional<Account> findAccountByUsernameAndAndPassword(String username, String password);
    boolean existsAccountByUsername(String username);
}
