package com.example.residencecms.sys.model.resident;

import com.example.residencecms.sys.model.population.Mounth;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "ABSENT")
@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
@SequenceGenerator(name = "ACCOUNT_SEQUENCE", sequenceName = "ACCOUNT_SEQUENCE", allocationSize = 1)
public class Absent {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ACCOUNT_SEQUENCE")
    private Integer id;
    private String profession;
    private String absentAddress;
    private String absentReason;
    private String accompanyChild;
    private String houseHolder;
    private String holderRelation;
    private Date fromTime;
    private Date toTime;
    private Integer officerConfirm;
    private Integer holderConfirm;

    @ManyToOne
    @JoinColumn(name = "MOUTH_ID")
    private Mounth mounth;
}
