package com.example.residencecms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ResidenceCmsApplication {

    public static void main(String[] args) {
        SpringApplication.run(ResidenceCmsApplication.class, args);
    }

}
