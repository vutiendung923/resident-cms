package com.example.residencecms.sys.service.impl;

import com.example.residencecms.common.PageData;
import com.example.residencecms.sys.model.resident.ToStay;
import com.example.residencecms.sys.repository.ToStayRepo;
import com.example.residencecms.sys.service.ToStayService;
import com.example.residencecms.sys.service.dto.resident.ToStayDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ToStayServiceImpl implements ToStayService {

    private final ToStayRepo toStayRepo;

    public ToStayServiceImpl(ToStayRepo toStayRepo) {
        this.toStayRepo = toStayRepo;
    }

    @Override
    @Transactional
    public PageData<ToStayDto> getListToStay(Integer pageIndex, Integer pageSize) {
        Specification<ToStay> specification = (root, cq, cb) -> {
            return cb.and();
        };
        Page<ToStay> toStayPage = toStayRepo.findAll(specification, PageRequest.of(pageIndex-1, pageSize));
        List<ToStayDto> toStayDtoList = toStayPage.getContent()
                .stream()
                .map(toStay -> ToStayDto.builder().toStay(toStay).build())
                .collect(Collectors.toList());
        return PageData.of(toStayPage, toStayDtoList);
    }

    @Override
    @Transactional
    public ToStayDto getDetailToStay(Integer id) {
        return ToStayDto.builder().toStay(
                toStayRepo.findById(id).orElseThrow(RuntimeException::new)).build();
    }

    @Override
    @Transactional
    public void addToStay(ToStayDto toStayDto) {
        ToStay toStay = new ToStay();
        toStayRepo.save(toStay);
    }

    @Override
    @Transactional
    public void updateToStay(ToStayDto toStayDto) {
        ToStay toStay = toStayRepo.findById(toStayDto.getId()).orElseThrow(RuntimeException::new);
    }

    @Override
    @Transactional
    public void deleteToStay(Integer id) {
        toStayRepo.deleteById(id);
    }

}
