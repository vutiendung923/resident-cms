package com.example.residencecms.config.filter;

import com.example.residencecms.common.Auth;
import com.example.residencecms.common.ResponseEntity;
import com.example.residencecms.common.ResponseTypes;
import com.example.residencecms.config.CustomSecurityContext;
import com.example.residencecms.sys.service.JwtService;
import com.google.gson.Gson;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.annotation.Priority;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.PreMatching;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.ext.Provider;
import java.io.IOException;
import java.util.Date;

import static com.example.residencecms.common.Constant.TRACING_LOG_ATTRIBUTE_NAME;


@Provider
@Priority(Priorities.AUTHENTICATION)
@PreMatching
public class AuthFilter implements ContainerRequestFilter {
    protected Log log = LogFactory.getLog(this.getClass());

    @Context
    private HttpServletRequest httpServletRequest;

    @Inject
    private JwtService jwtService;

    private static final String REALM = "xdevs";
    private static final String AUTHENTICATION_SCHEME = "Bearer";

    /**
     * Extracting the token from the request and validating it
     * <p>
     * The client should send the token in the standard HTTP Authorization header of
     * the request. For example: Authorization: Bearer <token-goes-here>
     * <p>
     * Finally, set the security context of the current request
     */
    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {
        // (1) Get Token Authorization from the header
        String authorizationHeader = requestContext.getHeaderString(HttpHeaders.AUTHORIZATION);

        // (2) Validate the Authorization header
        if (!isTokenBasedAuthentication(authorizationHeader)) {
            return;
        }

        // (3) Extract the token from the Authorization header
        String token = authorizationHeader.substring(AUTHENTICATION_SCHEME.length()).trim();

        try {

            // (4) Validate the token
            if (jwtService.isExpiredToken(token)) {
                abortWithUnauthorized(requestContext);
                return;
            }

            // (5) Getting the User information from token
            Auth user = jwtService.decode(token);
            log.info(String.format("requestId: %s, AuthInfo: %s", httpServletRequest.getAttribute(TRACING_LOG_ATTRIBUTE_NAME), new Gson().toJson(user)));
            // (6) Overriding the security context of the current request
            SecurityContext oldContext = requestContext.getSecurityContext();
            requestContext.setSecurityContext(new CustomSecurityContext(user, oldContext.isSecure()));
            requestContext.setProperty("user", user);

        } catch (Exception e) {
            log.debug(String.format("requestId: %s, TOKEN INVALID !", e));
            abortWithUnauthorized(requestContext);
        }
    }

    private boolean isTokenBasedAuthentication(String authorizationHeader) {
        // Check if the Authorization header is valid
        // It must not be null and must be prefixed with "Bearer" plus a whitespace
        // The authentication scheme comparison must be case-insensitive
        return authorizationHeader != null
                && authorizationHeader.toLowerCase().startsWith(AUTHENTICATION_SCHEME.toLowerCase() + " ");
    }

    private void abortWithUnauthorized(ContainerRequestContext requestContext) {
        String requestId = (String) httpServletRequest.getAttribute(TRACING_LOG_ATTRIBUTE_NAME);
        Response response = Response.status(Response.Status.OK)
                .header(HttpHeaders.WWW_AUTHENTICATE, AUTHENTICATION_SCHEME + " realm=\"" + REALM + "\"")
                .entity(new ResponseEntity(requestId, new Date(), ResponseTypes.TOKEN_INVALID)) // the response entity
                .build();
        requestContext.abortWith(response);
    }
}
