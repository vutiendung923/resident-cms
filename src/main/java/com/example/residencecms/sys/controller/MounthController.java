package com.example.residencecms.sys.controller;

import com.example.residencecms.common.ResponseEntity;
import com.example.residencecms.common.ResponseTypes;
import com.example.residencecms.config.inject.CurrentRequestId;
import com.example.residencecms.sys.service.MounthService;
import com.example.residencecms.sys.service.dto.population.MounthDto;
import org.springframework.stereotype.Component;

import javax.validation.Valid;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import java.util.Date;

@Component
@Valid
@Path("mounth")
public class MounthController {

    @CurrentRequestId
    String requestId;

    private final MounthService mounthService;

    public MounthController(MounthService mounthService) {
        this.mounthService = mounthService;
    }

    @GET
    @Path("list")
    public Response getListMounth(@QueryParam("pageIndex") Integer pageIndex,
                                  @QueryParam("pageSize") Integer pageSize){
        return Response.ok(new ResponseEntity(requestId, new Date(),
                ResponseTypes.SUCCESS, mounthService.getListMounth(pageIndex,pageSize))).build();
    }

    @GET
    @Path("detail")
    public Response getDetailMounth(@QueryParam("id") Integer id){
        return Response.ok(new ResponseEntity(requestId, new Date(),
                ResponseTypes.SUCCESS, mounthService.getDetailMounth(id))).build();
    }

    @POST
    @Path("add")
    public Response addMounth(MounthDto mounthDto){
        mounthService.addMounth(mounthDto);
        return Response.ok(new ResponseEntity(requestId, new Date(),
                ResponseTypes.SUCCESS)).build();
    }

    @POST
    @Path("update")
    public Response updateMounth(MounthDto mounthDto){
        mounthService.updateMounth(mounthDto);
        return Response.ok(new ResponseEntity(requestId, new Date(),
                ResponseTypes.SUCCESS)).build();
    }

    @GET
    @Path("delete")
    public Response deleteToStay(@QueryParam("id") Integer id){
        mounthService.deleteMounth(id);
        return Response.ok(new ResponseEntity(requestId, new Date(),
                ResponseTypes.SUCCESS)).build();
    }


}
