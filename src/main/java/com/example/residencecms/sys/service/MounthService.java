package com.example.residencecms.sys.service;

import com.example.residencecms.common.PageData;
import com.example.residencecms.sys.model.population.Mounth;
import com.example.residencecms.sys.service.dto.population.MounthDto;

public interface MounthService {
    PageData<MounthDto> getListMounth(Integer pageIndex, Integer pageSize);
    MounthDto getDetailMounth(Integer id);
    void addMounth(MounthDto mounthDto);
    void updateMounth(MounthDto mounthDto);
    void deleteMounth(Integer id);
}
