package com.example.residencecms.sys.service.impl;

import com.example.residencecms.common.PageData;
import com.example.residencecms.sys.model.population.Mounth;
import com.example.residencecms.sys.model.resident.Temporary;
import com.example.residencecms.sys.repository.TemporaryRepo;
import com.example.residencecms.sys.service.TemporaryService;
import com.example.residencecms.sys.service.dto.resident.TemporaryDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class TemporaryServiceImpl implements TemporaryService {

    private final TemporaryRepo temporaryRepo;

    public TemporaryServiceImpl(TemporaryRepo temporaryRepo) {
        this.temporaryRepo = temporaryRepo;
    }

    @Override
    @Transactional
    public PageData<TemporaryDto> getListTemporary(Integer pageIndex, Integer pageSize) {
        Specification<Temporary> specification = (root, cq, cb) -> {
            return cb.and();
        };
        Page<Temporary> temporaryPage = temporaryRepo.findAll(specification,
                PageRequest.of(pageIndex-1, pageSize));
        List<TemporaryDto> temporaryDtos = temporaryPage.getContent()
                .stream()
                .map(temporary -> TemporaryDto.builder().temporary(temporary).build())
                .collect(Collectors.toList());
        return PageData.of(temporaryPage, temporaryDtos);
    }

    @Override
    @Transactional
    public TemporaryDto getDetailTemporary(Integer id) {
        return TemporaryDto.builder().temporary(
                temporaryRepo.findById(id).orElseThrow(RuntimeException::new)
        ).build();
    }

    @Override
    @Transactional
    public void addTemporary(TemporaryDto temporaryDto) {
        Temporary temporary = new Temporary();
        temporary.setMounth(new Mounth(temporaryDto.getMounth().getId()));
        temporary.setTempAddress(temporaryDto.getTempAddress());
        temporary.setTempReason(temporaryDto.getTemptReason());
        temporary.setHouseHolder(temporaryDto.getHouseHolder());
        temporary.setFromTime(temporaryDto.getFromTime());
        temporary.setToTime(temporaryDto.getToTime());
        temporary.setHolderConfirm(temporaryDto.getHolderConfirm());
        temporary.setOfficerConfirm(temporaryDto.getOfficerConfirm());
        temporary.setAccompanyChild(temporaryDto.getAccompanyChild());
        temporary.setHolderRelation(temporaryDto.getHolderRelation());
        temporaryRepo.save(temporary);
    }

    @Override
    @Transactional
    public void updateTemporary(TemporaryDto temporaryDto) {
        Temporary temporary = temporaryRepo.findById(temporaryDto.getId()).orElseThrow(RuntimeException::new);
        temporary.setMounth(new Mounth(temporaryDto.getMounth().getId()));
        temporary.setTempAddress(temporaryDto.getTempAddress());
        temporary.setTempReason(temporaryDto.getTemptReason());
        temporary.setHouseHolder(temporaryDto.getHouseHolder());
        temporary.setFromTime(temporaryDto.getFromTime());
        temporary.setToTime(temporaryDto.getToTime());
        temporary.setHolderConfirm(temporaryDto.getHolderConfirm());
        temporary.setOfficerConfirm(temporaryDto.getOfficerConfirm());
        temporary.setAccompanyChild(temporaryDto.getAccompanyChild());
        temporary.setHolderRelation(temporaryDto.getHolderRelation());
        temporaryRepo.save(temporary);
    }

    @Override
    @Transactional
    public void deleteTemporary(Integer id) {
        temporaryRepo.deleteById(id);
    }
}
