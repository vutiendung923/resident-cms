package com.example.residencecms.sys.repository;

import com.example.residencecms.sys.model.resident.Temporary;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface TemporaryRepo extends JpaRepository<Temporary, Integer>, JpaSpecificationExecutor<Temporary> {
    void deleteTemporaryByMounth_Id(Integer mounthId);
}
