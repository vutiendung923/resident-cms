package com.example.residencecms.config;

import com.example.residencecms.config.filter.*;
import com.example.residencecms.exception.mapper.ConstraintViolationExceptionMapper;
import com.example.residencecms.exception.mapper.GenericExceptionMapper;
import com.example.residencecms.sys.controller.*;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.ServerProperties;
import org.glassfish.jersey.server.filter.RolesAllowedDynamicFeature;
import org.springframework.stereotype.Component;
import sun.net.httpserver.AuthFilter;

@Component
public class JerseyConfig extends ResourceConfig {
    public JerseyConfig() {
        register(AccountController.class);
        register(AbsentController.class);
        register(PopulationController.class);
        register(TemporaryController.class);
        register(MounthController.class);

        register(AuthFilter.class);
        register(ClientLoggingFilter.class);
        register(IpFilterImpl.class);
        register(OneTpsImpl.class);
        register(CustomLoggingRequestFilter.class);
        register(CustomLoggingResponseFilter.class);

        register(ConstraintViolationExceptionMapper.class);
//        register(DaoExceptionMapper.class);
        register(GenericExceptionMapper.class);

        register(RolesAllowedDynamicFeature.class);
        property(ServerProperties.BV_SEND_ERROR_IN_RESPONSE, true);
        property(ServerProperties.BV_DISABLE_VALIDATE_ON_EXECUTABLE_OVERRIDE_CHECK, true);
        register(BeanConfig.class);
        register(JsonConfig.class);
    }
}
