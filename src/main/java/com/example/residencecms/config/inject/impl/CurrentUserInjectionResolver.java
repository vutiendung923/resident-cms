package com.example.residencecms.config.inject.impl;

import com.example.residencecms.common.Auth;
import com.example.residencecms.config.inject.CurrentAuth;
import org.glassfish.hk2.api.Injectee;
import org.glassfish.hk2.api.InjectionResolver;
import org.glassfish.hk2.api.ServiceHandle;

import javax.inject.Inject;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.ext.Provider;

@Provider
public class CurrentUserInjectionResolver implements InjectionResolver<CurrentAuth> {

    @Inject
    private javax.inject.Provider<ContainerRequestContext> requestContext;

    @Override
    public Object resolve(Injectee injectee, ServiceHandle<?> sh) {
        if (Auth.class == injectee.getRequiredType()) {
            return requestContext.get().getProperty("user");
        }
        return null;
    }

    @Override
    public boolean isConstructorParameterIndicator() {
        return false;
    }

    @Override
    public boolean isMethodParameterIndicator() {
        return false;
    }
}
