package com.example.residencecms.sys.controller;

import com.example.residencecms.common.ResponseEntity;
import com.example.residencecms.common.ResponseTypes;
import com.example.residencecms.config.inject.CurrentRequestId;
import com.example.residencecms.sys.service.ToStayService;
import com.example.residencecms.sys.service.dto.resident.ToStayDto;
import org.springframework.stereotype.Component;

import javax.validation.Valid;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import java.util.Date;

@Component
@Valid
@Path("to-stay")
public class ToStayController {

    @CurrentRequestId
    String requestId;

    private final ToStayService toStayService;

    public ToStayController(ToStayService toStayService) {
        this.toStayService = toStayService;
    }

    @GET
    @Path("list")
    public Response getListToStay(@QueryParam("pageIndex") Integer pageIndex,
                                  @QueryParam("pageSize") Integer pageSize){
        return Response.ok(new ResponseEntity(requestId, new Date(),
                ResponseTypes.SUCCESS, toStayService.getListToStay(pageIndex, pageSize))).build();
    }

    @GET
    @Path("detail")
    public Response getDetailToStay(@QueryParam("id") Integer id){
        return Response.ok(new ResponseEntity(requestId, new Date(),
                ResponseTypes.SUCCESS, toStayService.getDetailToStay(id))).build();
    }

    @POST
    @Path("add")
    public Response addToStay(ToStayDto toStayDto){
        toStayService.addToStay(toStayDto);
        return Response.ok(new ResponseEntity(requestId, new Date(),
                ResponseTypes.SUCCESS)).build();
    }

    @POST
    @Path("update")
    public Response updateToStay(ToStayDto toStayDto){
        toStayService.updateToStay(toStayDto);
        return Response.ok(new ResponseEntity(requestId, new Date(),
                ResponseTypes.SUCCESS)).build();
    }

    @GET
    @Path("delêt")
    public Response deleteToStay(@QueryParam("id") Integer id){
        toStayService.deleteToStay(id);
        return Response.ok(new ResponseEntity(requestId, new Date(),
                ResponseTypes.SUCCESS)).build();
    }
}
