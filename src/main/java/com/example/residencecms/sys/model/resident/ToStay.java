package com.example.residencecms.sys.model.resident;

import com.example.residencecms.sys.model.population.Mounth;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;

@Entity
@Table(name = "TO_STAY")
@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
@SequenceGenerator(name = "SEQ", sequenceName = "SEQ", allocationSize = 1)
public class ToStay {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ")
    private Integer id;

//    @ManyToOne
//    @JoinColumn(name = "MOUNTH_ID")
//    private Mounth mounth;
}
