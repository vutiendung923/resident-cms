package com.example.residencecms.sys.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "GROUP_ACCOUNT")
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Data
@SequenceGenerator(name = "SEQ", sequenceName = "SEQ", allocationSize = 1)
public class GroupAccount {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ")
    private Integer id;
    private String groupName;

    @OneToMany(mappedBy = "groupAccount")
    private List<Account> account;
}
