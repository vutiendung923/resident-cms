package com.example.residencecms.sys.repository;

import com.example.residencecms.sys.model.population.Population;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface PopulationRepo extends JpaRepository<Population, Integer>, JpaSpecificationExecutor<Population> {
}
