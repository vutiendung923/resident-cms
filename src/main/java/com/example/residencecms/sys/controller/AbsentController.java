package com.example.residencecms.sys.controller;

import com.example.residencecms.common.ResponseEntity;
import com.example.residencecms.common.ResponseTypes;
import com.example.residencecms.config.inject.CurrentRequestId;
import com.example.residencecms.sys.service.AbsentService;
import com.example.residencecms.sys.service.dto.resident.AbsentDto;
import org.springframework.stereotype.Component;

import javax.validation.Valid;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import java.util.Date;

@Component
@Valid
@Path("absent")
public class AbsentController {

    @CurrentRequestId
    String requestId;

    private final AbsentService absentService;

    public AbsentController(AbsentService absentService) {
        this.absentService = absentService;
    }

    @GET
    @Path("list")
    public Response getListAbsent(@QueryParam("pageIndex") Integer pageIndex,
                                  @QueryParam("pageSize") Integer pageSize) {
        return Response.ok(new ResponseEntity(requestId, new Date(),
                ResponseTypes.SUCCESS, absentService.getListAbsent(pageIndex, pageSize))).build();
    }

    @GET
    @Path("detail")
    public Response getDetailAbsent(@QueryParam("id") Integer id) {
        return Response.ok(new ResponseEntity(requestId, new Date(),
                ResponseTypes.SUCCESS, absentService.getDetailAbsent(id))).build();
    }

    @POST
    @Path("add")
    public Response addAbsent(AbsentDto absentDto) {
        absentService.addAbsent(absentDto);
        return Response.ok(new ResponseEntity(requestId, new Date(),
                ResponseTypes.SUCCESS)).build();
    }

    @POST
    @Path("update")
    public Response updateAbsent(AbsentDto absentDto) {
        absentService.updateAbsent(absentDto);
        return Response.ok(new ResponseEntity(requestId, new Date(),
                ResponseTypes.SUCCESS)).build();
    }

    @GET
    @Path("delete")
    public Response deleteAbsent(@QueryParam("id") Integer id) {
        absentService.deleteAbsent(id);
        return Response.ok(new ResponseEntity(requestId, new Date(),
                ResponseTypes.SUCCESS)).build();
    }

}
