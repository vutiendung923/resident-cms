package com.example.residencecms.exception.mapper;

import com.example.residencecms.common.Constant;
import com.example.residencecms.common.ResponseEntity;
import com.example.residencecms.common.ResponseTypes;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import java.util.Date;

public class ConstraintViolationExceptionMapper implements ExceptionMapper<ConstraintViolationException> {
    @Context
    private HttpServletRequest httpServletRequest;

    private final Log log = LogFactory.getLog(this.getClass());

    @Override
    public Response toResponse(ConstraintViolationException exception) {
        String requestId = (String) httpServletRequest.getAttribute(Constant.TRACING_LOG_ATTRIBUTE_NAME);
        log.error(String.format("requestId: %s, [CONSTRAINT VIOLATION EXCEPTION] ==> %s", requestId, exception.getMessage()), exception);
        return Response.status(Response.Status.BAD_REQUEST)
                .entity(new ResponseEntity(requestId, new Date(), ResponseTypes.BAD_REQUEST, prepareMessage(exception), exception))
                .type(MediaType.APPLICATION_JSON_TYPE)
                .build();
    }

    private String prepareMessage(ConstraintViolationException exception) {
        StringBuilder message = new StringBuilder();
        for (ConstraintViolation<?> cv : exception.getConstraintViolations()) {
            message.append(cv.getMessage()).append(", ");
        }
        return message.toString();
    }
}
