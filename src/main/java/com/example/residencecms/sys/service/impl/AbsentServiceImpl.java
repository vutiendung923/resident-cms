package com.example.residencecms.sys.service.impl;

import com.example.residencecms.common.PageData;
import com.example.residencecms.sys.model.population.Mounth;
import com.example.residencecms.sys.model.resident.Absent;
import com.example.residencecms.sys.repository.AbsentRepo;
import com.example.residencecms.sys.service.AbsentService;
import com.example.residencecms.sys.service.dto.resident.AbsentDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class AbsentServiceImpl implements AbsentService {

    private final AbsentRepo absentRepo;

    public AbsentServiceImpl(AbsentRepo absentRepo) {
        this.absentRepo = absentRepo;
    }

    @Override
    @Transactional
    public PageData<AbsentDto> getListAbsent(Integer pageIndex, Integer pageSize) {
        Specification<Absent> specification = (root, cq, cb) -> {
          return cb.and();
        };
        Page<Absent> absentPage = absentRepo.findAll(specification,
                PageRequest.of(pageIndex-1,pageSize));
        List<AbsentDto> absentDtos = absentPage.getContent()
                .stream()
                .map(absent -> AbsentDto.builder().absent(absent).build())
                .collect(Collectors.toList());
        return PageData.of(absentPage, absentDtos);
    }

    @Override
    @Transactional
    public AbsentDto getDetailAbsent(Integer id) {
        return AbsentDto.builder().absent(
                absentRepo.findById(id).orElseThrow(RuntimeException::new)).build();
    }

    @Override
    @Transactional
    public void addAbsent(AbsentDto absentDto) {
        Absent absent = new Absent();
        absent.setMounth(new Mounth(absentDto.getMounth().getId()));
        absent.setAbsentAddress(absentDto.getAbsentAddress());
        absent.setAccompanyChild(absentDto.getAccompanyChild());
        absent.setAbsentReason(absentDto.getAbsentReason());
        absent.setProfession(absentDto.getProfession());
        absent.setHolderRelation(absentDto.getHolderRelation());
        absent.setFromTime(absentDto.getFromTime());
        absent.setToTime(absentDto.getToTime());
        absent.setHolderConfirm(absentDto.getHolderConfirm());
        absent.setOfficerConfirm(absentDto.getOfficerConfirm());
        absentRepo.save(absent);
    }

    @Override
    @Transactional
    public void updateAbsent(AbsentDto absentDto) {
        Absent absent = absentRepo.findById(absentDto.getId()).orElseThrow(RuntimeException::new);
        absent.setMounth(new Mounth(absentDto.getMounth().getId()));
        absent.setAbsentAddress(absentDto.getAbsentAddress());
        absent.setAccompanyChild(absentDto.getAccompanyChild());
        absent.setAbsentReason(absentDto.getAbsentReason());
        absent.setProfession(absentDto.getProfession());
        absent.setHolderRelation(absentDto.getHolderRelation());
        absent.setFromTime(absentDto.getFromTime());
        absent.setToTime(absentDto.getToTime());
        absent.setHolderConfirm(absentDto.getHolderConfirm());
        absent.setOfficerConfirm(absentDto.getOfficerConfirm());
        absentRepo.save(absent);
    }

    @Override
    @Transactional
    public void deleteAbsent(Integer id) {
        absentRepo.deleteById(id);
    }
}
