package com.example.residencecms.sys.service.impl;

import com.example.residencecms.common.Auth;
import com.example.residencecms.common.PageData;
import com.example.residencecms.common.Roles;
import com.example.residencecms.root.Account_;
import com.example.residencecms.sys.model.Account;
import com.example.residencecms.sys.repository.AccountRepo;
import com.example.residencecms.sys.service.AccountService;
import com.example.residencecms.sys.service.JwtService;
import com.example.residencecms.sys.service.dto.AccountDto;
import com.example.residencecms.sys.service.dto.LoginResponseDto;
import com.example.residencecms.utils.CryptoUtils;
import com.example.residencecms.utils.QueryUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class AccountServiceImpl implements AccountService {

    private final AccountRepo accountRepo;
    private final JwtService jwtService;

    public AccountServiceImpl(AccountRepo accountRepo, JwtService jwtService) {
        this.accountRepo = accountRepo;
        this.jwtService = jwtService;
    }

    @Override
    public void addAccount(AccountDto accountDto) {
        Account account = new Account();
        if(accountRepo.existsAccountByUsername(accountDto.getUsername())){
            throw null;
        }else {
            account.setUsername(accountDto.getUsername());
        }
        account.setPassword(CryptoUtils.encodeMD5(accountDto.getPassword()));
        account.setAccountStatus(accountDto.getAccountStatus());
        accountRepo.save(account);
    }

    @Override
    @Transactional
    public LoginResponseDto loginAccount(AccountDto accountDto) {
        String username = accountDto.getUsername();
        String password = accountDto.getPassword();
        Account account = accountRepo.findAccountByUsernameAndAndPassword(username,
                CryptoUtils.encodeMD5(password))
                .orElseThrow(RuntimeException::new);
        Auth auth = new Auth();
        auth.setId(account.getId());
        auth.setUserName(account.getUsername());
        auth.setRoles(Collections.singletonList(Roles.ADMIN));
        String token = jwtService.encode(auth, 1);
        LoginResponseDto loginResponseDto = new LoginResponseDto();
        loginResponseDto.setId(account.getId());
        loginResponseDto.setUsername(account.getUsername());
        loginResponseDto.setRole(Roles.ADMIN);
        loginResponseDto.setToken(token);
        return loginResponseDto;
    }

    @Override
    @Transactional
    public PageData<AccountDto> getListAccount(Integer pageIndex, Integer pageSize, String username, Integer accountStatus) {
        Specification<Account> specification = (root, cq, cb) -> (
                QueryUtils.buildLikeFilter(root, cb, username, Account_.USERNAME)
                );
        Page<Account> accountPage = accountRepo.findAll(specification, PageRequest.of(pageIndex-1, pageSize));
        List<AccountDto> accountDtoList = accountPage.getContent()
                .stream()
                .map(account -> AccountDto.builder().account(account).build())
                .collect(Collectors.toList());
        return PageData.of(accountPage, accountDtoList);
    }

    @Override
    @Transactional
    public AccountDto getDetailAccount(Integer id) {
        return AccountDto.builder().account(accountRepo.findById(id).orElseThrow(RuntimeException::new)).build();
    }

    @Override
    public void updateAccount(AccountDto accountDto) {
        Integer id  = accountDto.getId();
        Account account = accountRepo.findById(id).orElseThrow(RuntimeException::new);
        account.setUsername(accountDto.getUsername());
        account.setPassword(CryptoUtils.encodeMD5(accountDto.getPassword()));
        account.setAccountStatus(accountDto.getAccountStatus());
        accountRepo.save(account);
    }

    @Override
    public void deleteAccount(Integer id) {
        accountRepo.deleteById(id);
    }
}
