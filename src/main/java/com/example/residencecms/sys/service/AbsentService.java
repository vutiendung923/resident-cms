package com.example.residencecms.sys.service;

import com.example.residencecms.common.PageData;
import com.example.residencecms.sys.service.dto.resident.AbsentDto;

public interface AbsentService {
    PageData<AbsentDto> getListAbsent(Integer pageIndex, Integer pageSize);
    AbsentDto getDetailAbsent(Integer id);
    void addAbsent(AbsentDto absentDto);
    void updateAbsent(AbsentDto absentDto);
    void deleteAbsent(Integer id);
}
